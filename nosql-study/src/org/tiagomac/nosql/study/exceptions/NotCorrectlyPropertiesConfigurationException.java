package org.tiagomac.nosql.study.exceptions;

public class NotCorrectlyPropertiesConfigurationException extends Exception {

	private static final long serialVersionUID = 4906664760833933956L;

	@Override
	public String getMessage() {
		return super.getMessage() + " N�o foi poss�vel carregar o valor no arquivo de properties corretamente.";
	}

	
}
