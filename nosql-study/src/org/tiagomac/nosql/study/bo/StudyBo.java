package org.tiagomac.nosql.study.bo;

import java.util.List;
import java.util.Properties;

import org.tiagomac.nosql.study.exceptions.NotCorrectlyPropertiesConfigurationException;
import org.tiagomac.nosql.study.model.beans.Usuario;
import org.tiagomac.nosql.study.model.dao.ClienteDao;
import org.tiagomac.nosql.study.model.dao.EnderecoDao;
import org.tiagomac.nosql.study.model.dao.LocalDao;
import org.tiagomac.nosql.study.model.dao.MensagemDao;
import org.tiagomac.nosql.study.model.dao.UsuarioDao;
import org.tiagomac.nosql.study.model.dao.impl.mysql.ClienteDaoMysqlImpl;
import org.tiagomac.nosql.study.model.dao.impl.mysql.EnderecoDaoImpl;
import org.tiagomac.nosql.study.model.dao.impl.mysql.LocalDaoImpl;
import org.tiagomac.nosql.study.model.dao.impl.mysql.MensagemDaoImpl;
import org.tiagomac.nosql.study.model.dao.impl.mysql.UsuarioDaoImpl;
import org.tiagomac.nosql.study.utils.Creator;
import org.tiagomac.nosql.study.utils.Timer;

/**
 * Business Object
 * @author tiagocunha
 *
 */
public class StudyBo {
	
	private EnderecoDao enderecoDao = null;
	private LocalDao localDao		= null;
	private MensagemDao mensagemDao	= null;
	private UsuarioDao usuarioDao	= null;
	private ClienteDao clienteDao	= null;
	
	private boolean mysql	=	false;
	private boolean mongodb	=	false;
	private final boolean TODOS 	= 	false;
	
	public StudyBo() throws Exception {
		
		Properties props = new Properties();
		String db = null;
		props.load(this.getClass().getResourceAsStream("/config.properties"));
        db = props.getProperty("database");
        if (db.equals("mysql")){
        	this.mysql = true;
        	this.enderecoDao = new EnderecoDaoImpl();
        	this.localDao = new LocalDaoImpl();
        	this.mensagemDao = new MensagemDaoImpl();
        	this.usuarioDao = new UsuarioDaoImpl();
        	this.clienteDao = new ClienteDaoMysqlImpl();
        }else if (db.equals("mongodb")){
        	this.mongodb = true;
        	this.enderecoDao = new org.tiagomac.nosql.study.model.dao.impl.mongodb.EnderecoDaoImpl();
        	this.localDao = new org.tiagomac.nosql.study.model.dao.impl.mongodb.LocalDaoImpl();
        	this.mensagemDao = new org.tiagomac.nosql.study.model.dao.impl.mongodb.MensagemDaoImpl();
        	this.usuarioDao = new org.tiagomac.nosql.study.model.dao.impl.mongodb.UsuarioDaoImpl();
        	this.clienteDao = new ClienteDaoMysqlImpl();
        }else if (db == null || db.isEmpty()){
        	throw new NotCorrectlyPropertiesConfigurationException();
        }
        
	}

	public void inserirUsuarios(long qtdUsuario, long qtdLocal, long qtdMensagem) throws Exception{

		List<Usuario> usuarios = Creator.getRandomUsuarios(qtdUsuario, qtdLocal, qtdMensagem);
		
		usuarioDao.inserirAll(usuarios);
		usuarioDao = new org.tiagomac.nosql.study.model.dao.impl.mongodb.UsuarioDaoImpl();
		usuarioDao.inserirAll(usuarios);
		
//		// mysql
//		if (mysql || TODOS){
//			usuarioDao.inserirAll(usuarios);
//			System.out.println("Mysql insert Total: "+Timer.result());
//		}
//
//		// mongodb
//		if (mongodb || TODOS){
//			Timer.start();
//			usuarioDao.inserirAll(usuarios);
//			Timer.end();
//			System.out.println("Mongodb insert mongodb: "+Timer.result());
//		}
	}
	
	public void deletarTodosUsuarios(){
		
		//mysql
		if (mysql){
			usuarioDao.deleteAll();
			System.out.println("Mysql delete total: "+Timer.result());
		}
		if (mongodb){
			usuarioDao.deleteAll();
			System.out.println("Mongodb delete total: "+Timer.result());
		}
	}
	
	public void getPrimeiros(int qntd){
		
		if (mysql){
			System.out.println("not yet implemented!");
			System.out.println("Total mysql: "+Timer.result());
			// TODO: impl MySQL.
		}
		if (mongodb){
			usuarioDao.findPrimeiros(qntd);
			System.out.println("Total mongodb: "+Timer.result());
		}
		
	}
	
	public void getPorLogin(String... nomes){
		if (mysql){
			//TODO: impl MySQL.
		}
		if (mongodb){
			usuarioDao.getPorLogin(nomes);
			System.out.println("Total mongodb: "+Timer.result());
		}
	}

	public void getSelect01() {
		usuarioDao.getSelect01();
	}

	public void update01() {
		usuarioDao.update01();
	}

}
