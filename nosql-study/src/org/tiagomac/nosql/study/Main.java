package org.tiagomac.nosql.study;

import java.util.List;
import java.util.Scanner;

import org.tiagomac.nosql.study.model.beans.Usuario;
import org.tiagomac.nosql.study.model.dao.UsuarioDao;
import org.tiagomac.nosql.study.utils.Creator;

public class Main {
	private static int erro = 0;

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		UsuarioDao usuarioDao = null;
		Scanner s = new Scanner(System.in);
//		while (s.next() != null){
//		}
				
//		while (true){
			List<Usuario> usuarios = Creator.getRandomUsuarios(100, 10, 10);
			long startTime = System.currentTimeMillis();
		
			// mysql
			usuarioDao = new org.tiagomac.nosql.study.model.dao.impl.mysql.UsuarioDaoImpl();
			usuarioDao.inserirAll(usuarios);
			long endMysql = System.currentTimeMillis();
			
			// mongodb
			usuarioDao = new org.tiagomac.nosql.study.model.dao.impl.mongodb.UsuarioDaoImpl();
			usuarioDao.inserirAll(usuarios);
			long endMongodb = System.currentTimeMillis();
			
			long totalMysql = 	(endMysql-startTime);
			long totalMongodb = (endMongodb-endMysql);
			
			System.out.println("total mysql: "+totalMysql+" seg");
			System.out.println("total mongodb: "+totalMongodb+" seg");
//		}
		
		// for (int i = 0; i < 200; i++) {
		// new Thread() {
		//
		// public void run() {
		// try {
		// UsuarioDao usuarioDao = new UsuarioDaoImpl();
		// Usuario usuario = new Usuario();
		// usuario.setCriacao(12111986l);
		// usuario.setEmail("tiagomac@gmail.com");
		// usuario.setLogin("tiagomac");
		// usuario.setSenha("123123");
		// usuario.setTimestamp((new Date()).getTime());
		//
		// List<Usuario> amigos = new ArrayList<Usuario>();
		// amigos.add(usuario);
		// usuario.setAmigos(amigos);
		//
		// Endereco endereco = new Endereco();
		// endereco.setCep("40280-070");
		// endereco.setCidade("Salvador");
		// endereco.setComplemento("Complemento");
		// endereco.setEstado("Estado");
		// endereco.setEstado("Bahia");
		// endereco.setLogadouro("Logadouro");
		// endereco.setPais("Brasil");
		// endereco.setUsuario(usuario);
		//
		// usuario.setEndereco(endereco);
		//
		// Local local = new Local();
		// local.setLatitude("4574");
		// local.setLongitude("4575");
		// local.setTimestamp(46655l);
		// local.setUsuario(usuario);
		// Local l2 = new Local();
		// l2.setLatitude("5555");
		// l2.setLongitude("6666");
		// l2.setTimestamp(46655l);
		// l2.setUsuario(usuario);
		// List<Local> locais = new ArrayList<Local>();
		// locais.add(local);
		// locais.add(l2);
		//
		// usuario.setLocal(locais);
		//
		// Mensagem mensagem = new Mensagem();
		// mensagem.setTexto("texto mensagem");
		// mensagem.setTimestamp(546546L);
		// mensagem.setUsuario(usuario);
		// Mensagem m2 = new Mensagem();
		// m2.setTexto("texto mensagem 2");
		// m2.setTimestamp(333336L);
		// m2.setUsuario(usuario);
		// List<Mensagem> mensagens = new ArrayList<Mensagem>();
		// mensagens.add(mensagem);
		// mensagens.add(m2);
		//
		// usuario.setMensagem(mensagens);
		// // while (true) {
		// // usuarioDao.inserir(usuario);
		// // usuarioDao.inserir(usuario);
		// // usuarioDao.inserir(usuario);
		// // usuarioDao.inserir(usuario);
		// // usuarioDao.inserir(usuario);
		// // usuarioDao.inserir(usuario);
		// // usuarioDao.inserir(usuario);
		// // }
		// usuarioDao.listar();
		// } catch (Exception e) {
		// Main.erro++;
		// System.out.println(Main.erro);
		// }
		// }
		//
		// }.start();
		// }

	}

}
