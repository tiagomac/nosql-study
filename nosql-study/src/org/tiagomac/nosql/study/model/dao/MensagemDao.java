package org.tiagomac.nosql.study.model.dao;

import org.tiagomac.nosql.study.model.beans.Mensagem;

public interface MensagemDao extends DaoGenerico<Mensagem> {}
