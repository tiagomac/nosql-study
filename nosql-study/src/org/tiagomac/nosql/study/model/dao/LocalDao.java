package org.tiagomac.nosql.study.model.dao;

import org.tiagomac.nosql.study.model.beans.Local;

public interface LocalDao extends DaoGenerico<Local>{}
