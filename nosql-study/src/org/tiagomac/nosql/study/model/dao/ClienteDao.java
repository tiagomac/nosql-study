package org.tiagomac.nosql.study.model.dao;

import org.tiagomac.nosql.study.model.beans.Cliente;

public interface ClienteDao extends DaoGenerico<Cliente> {}
