package org.tiagomac.nosql.study.model.dao.impl.mongodb;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import org.tiagomac.nosql.study.model.beans.Usuario;
import org.tiagomac.nosql.study.model.dao.UsuarioDao;
import org.tiagomac.nosql.study.model.dao.connection.MongoDBConnection;
import org.tiagomac.nosql.study.utils.Timer;

import com.mongodb.BasicDBObject;
import com.mongodb.BasicDBObjectBuilder;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoException;
import com.mongodb.WriteResult;

public class UsuarioDaoImpl implements UsuarioDao {
	
	private MongoDBConnection con = null;
	
	public UsuarioDaoImpl(){
		try {
			this.con = MongoDBConnection.getInstance();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (MongoException e) {
			e.printStackTrace();
		}
	}

	@Override
	public boolean inserir(Usuario obj) {
		BasicDBObject dbo = obj.getBasicDBObject();
		con.getDB().requestStart();
		WriteResult rs = con.getDB().getCollection("usuario").insert(dbo);
		con.getDB().requestDone();
		return rs.getN() > 0;
	}
	
	@Override
	public boolean inserirAll(List<Usuario> obj) {
		List<DBObject> toIns = new ArrayList<DBObject>();
		for (Usuario u: obj){
			toIns.add(u.getBasicDBObject());
		}
		con.getDB().requestStart();
		WriteResult rs = con.getDB().getCollection("usuario").insert(toIns);
		con.getDB().requestDone();
		return rs.getN() > 0;
	}
	
	@Override
	public void deleteAll() {
		Timer.start();
		con.getDB().requestStart();
		con.getDB().getCollection("usuario").remove(new BasicDBObject());
		con.getDB().requestDone();
		Timer.end();
	}

	@Override
	public boolean atualizar(Usuario obj) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean excluir(Usuario obj) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public List<Usuario> pesquisar(Usuario obj) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Usuario> listar() {
		con.getDB().requestStart();
		DBCursor rs = con.getDB().getCollection("usuario").find();
		List<DBObject> lista = new ArrayList<DBObject>();
		while (rs.hasNext()){
			DBObject obj = rs.next();
			lista.add(obj);
		}
		con.getDB().requestDone();
//		ArrayList<>
		return null;
	}

	@Override
	public List<DBObject> findPrimeiros(int qntd) {
		Timer.start();
		con.getDB().requestStart();
		DBCursor cur = con.getDB().getCollection("usuario").find().limit(qntd);
		con.getDB().requestDone();
		Timer.end();
		List<DBObject> listReturn = new ArrayList<DBObject>();
		while (cur.hasNext()){
			DBObject obj = cur.next();
			listReturn.add(obj);
		}
		return listReturn;
	}

	@Override
	public void getPorLogin(String... nomes) {
		BasicDBObjectBuilder build = BasicDBObjectBuilder.start();
		BasicDBObject obNomes = new BasicDBObject("$in", nomes);
		build.add("login", obNomes);
		Timer.start();
		con.getDB().requestStart();
		DBCursor cur = con.getDB().getCollection("usuario").find(build.get());
		con.getDB().requestDone();
		Timer.end();
		List<DBObject> listReturn = new ArrayList<DBObject>();
		while (cur.hasNext()){
			DBObject obj = cur.next();
			listReturn.add(obj);
		}
		System.out.println("Encontrados "+listReturn.size());
	}

	@Override
	public void getSelect01() {
		BasicDBObjectBuilder build = BasicDBObjectBuilder.start();
		
		Timer.start();
		BasicDBObject oLongitude = new BasicDBObject("$regex", "091$");
		build.add("local.longitude", oLongitude);
		build.add("endereco.pais", "g");
		build.add("endereco.estado","f");
		BasicDBObject oTexto = new BasicDBObject("$regex", "i$");
		build.add("mensagens.texto", oTexto);
		con.getDB().requestStart();
		DBCursor cur = con.getDB().getCollection("usuario").find(build.get());
		con.getDB().requestDone();
		Timer.end();
		
		List<DBObject> listReturn = new ArrayList<DBObject>();
		while (cur.hasNext()){
			DBObject obj = cur.next();
			listReturn.add(obj);
		}
		int count = listReturn.size();
		System.out.println("Total mongodb: "+Timer.result()+" count: "+count);
	}

	@Override
	public void update01() {
		BasicDBObjectBuilder build = BasicDBObjectBuilder.start();
		BasicDBObjectBuilder buildTwo = BasicDBObjectBuilder.start();
		buildTwo.add("$set", new BasicDBObject("email","teste@teste.com.br"));
		BasicDBObject oTexto = new BasicDBObject("$regex", "s$");
		build.add("mensagens.texto", oTexto);
		build.add("endereco.pais", "e");
		Timer.start();
		con.getDB().requestStart();
		WriteResult wr = con.getDB().getCollection("usuario").
				updateMulti(build.get(), buildTwo.get());
		con.getDB().requestDone();
		Timer.end();
		System.out.println("Total mongodb: "+Timer.result());
	}

}
