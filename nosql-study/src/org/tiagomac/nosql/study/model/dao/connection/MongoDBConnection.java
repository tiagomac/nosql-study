package org.tiagomac.nosql.study.model.dao.connection;

import java.net.UnknownHostException;

import com.mongodb.DB;
import com.mongodb.Mongo;
import com.mongodb.MongoException;

/**
 * Singleton interface for get connectons with MongoDB.
 * @author tiagocunha
 */
public class MongoDBConnection {
	
	private static MongoDBConnection thisInstance = null;
	private static Mongo m = null;
	private DB db = null;
	
	private MongoDBConnection() throws UnknownHostException, MongoException{
		if (this.m == null)
			this.m = new Mongo("localhost",27017);
		if (this.db == null)
			this.db = m.getDB("test");
	}
	
	public static MongoDBConnection getInstance() throws UnknownHostException, MongoException{
		if (MongoDBConnection.thisInstance == null){
			MongoDBConnection.thisInstance = new MongoDBConnection();
		}
		return MongoDBConnection.thisInstance;
	}
	
	public DB getDB(){
		return this.db;
	}
	
}
