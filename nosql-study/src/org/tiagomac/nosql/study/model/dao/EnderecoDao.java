package org.tiagomac.nosql.study.model.dao;

import org.tiagomac.nosql.study.model.beans.Endereco;

public interface EnderecoDao extends DaoGenerico<Endereco> {}
