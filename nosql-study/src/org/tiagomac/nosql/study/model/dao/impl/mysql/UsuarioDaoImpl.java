package org.tiagomac.nosql.study.model.dao.impl.mysql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

import org.tiagomac.nosql.study.model.beans.Endereco;
import org.tiagomac.nosql.study.model.beans.Local;
import org.tiagomac.nosql.study.model.beans.Mensagem;
import org.tiagomac.nosql.study.model.beans.Usuario;
import org.tiagomac.nosql.study.model.dao.UsuarioDao;
import org.tiagomac.nosql.study.utils.Timer;

import com.mongodb.DBObject;

public class UsuarioDaoImpl implements UsuarioDao {
	
	private Connection conn = null;
	
	public UsuarioDaoImpl(){
		try {
			Class.forName ("com.mysql.jdbc.Driver").newInstance();
			this.conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/tcc", "root", "root");
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public boolean inserir(Usuario obj) {
		try {
			PreparedStatement psEndereco 	= this.conn.prepareStatement("");
			PreparedStatement psLocal 		= this.conn.prepareStatement("");
			PreparedStatement psMensagem 	= this.conn.prepareStatement("");
			PreparedStatement psUsuario 	= this.conn.prepareStatement("");
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public boolean inserirAll(List<Usuario> obj) {
		try {
			PreparedStatement psUsuario 	= this.conn.prepareStatement("INSERT INTO usuario (login, senha, email, timestamp, criacao) VALUES(?,?,?,?,?);");
			PreparedStatement psEndereco 	= this.conn.prepareStatement("INSERT INTO endereco (usuario_idusuario, pais, estado, cidade, cep, logadouro, complemento) VALUES(?,?,?,?,?,?,?);");
			PreparedStatement psLocal 		= this.conn.prepareStatement("INSERT INTO local (usuario_idusuario, latitude, longitude, timestamp) VALUES(?,?,?,?);");
			PreparedStatement psMensagem 	= this.conn.prepareStatement("INSERT INTO mensagem (usuario_idusuario, texto, timestamp) VALUES(?,?,?);");
			PreparedStatement psUsuarioRel 	= this.conn.prepareStatement("INSERT INTO usuario_has_usuario (usuario_iusuariod, usuario_idusuario_amigo) VALUES(?,?);");
			
			for (Usuario u:obj){
				long uIdLong = obj.lastIndexOf(u)+1;
				
//				psUsuario.setString(parameterIndex, x)
				psUsuario.setString(1, u.getLogin());
				psUsuario.setString(2, u.getSenha());
				psUsuario.setString(3, u.getEmail());
				psUsuario.setTimestamp(4, new Timestamp(u.getTimestamp()));
				psUsuario.setTimestamp(5, new Timestamp(u.getCriacao()));
				psUsuario.addBatch();
				
				Endereco end = u.getEndereco();
				psEndereco.setLong(1, uIdLong); // id do usu�rio.
				psEndereco.setString(2, end.getPais()	);
				psEndereco.setString(3, end.getEstado()	);
				psEndereco.setString(4, end.getCidade()	);
				psEndereco.setString(5, end.getCep()	);
				psEndereco.setString(6, end.getLogadouro());
				psEndereco.setString(7, end.getComplemento());
				psEndereco.addBatch();
				
				for (Local loc:u.getLocal()){
					psLocal.setLong(1, uIdLong);
					psLocal.setString(2, loc.getLatitude());
					psLocal.setString(3, loc.getLongitude());
					psLocal.setTimestamp(4, new Timestamp(loc.getTimestamp()));
					psLocal.addBatch();
				}
				
				for (Mensagem men:u.getMensagem()){
					psMensagem.setLong(1, uIdLong);
					psMensagem.setString(2, men.getTexto());
					psMensagem.setTimestamp(3, new Timestamp(men.getTimestamp()));
					psMensagem.addBatch();
				}
				
				Timer.start();
				psUsuario.executeBatch();
				psEndereco.executeBatch();
				psLocal.executeBatch();
				psMensagem.executeBatch();
				
				psUsuario.clearBatch();
				psEndereco.clearBatch();
				psLocal.clearBatch();
				psMensagem.clearBatch();
				Timer.end();
			}
			
			
		} catch (Exception e) {
			System.out.println("erro execu��o!");
			e.printStackTrace();
		}
		return false;
	}
	
	@Override
	public void deleteAll() {
		try{
			Timer.start();
			this.conn.createStatement().execute("delete from usuario_has_usuario;");
			this.conn.createStatement().execute("delete from local;");
			this.conn.createStatement().execute("delete from mensagem;");
			this.conn.createStatement().execute("delete from endereco;");
			this.conn.createStatement().execute("delete from usuario;");
			Timer.end();
			this.conn.createStatement().execute("ALTER TABLE usuario AUTO_INCREMENT = 1;");
			this.conn.createStatement().execute("ALTER TABLE local AUTO_INCREMENT = 1;");
			this.conn.createStatement().execute("ALTER TABLE mensagem AUTO_INCREMENT = 1;");
			this.conn.createStatement().execute("ALTER TABLE endereco AUTO_INCREMENT = 1;");
		}catch (Exception e){
			e.printStackTrace();
		}

		// TODO implementar o delete.
		
	}
	
	@Override
	public boolean atualizar(Usuario obj) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean excluir(Usuario obj) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public List<Usuario> pesquisar(Usuario obj) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Usuario> listar() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<DBObject> findPrimeiros(int qntd) {
		// TODO: impl mysql.
		return null;
	}

	@Override
	public void getPorLogin(String... nomes) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void getSelect01() {
		Timer.start();
		try {
			Timer.start();
			ResultSet rs = this.conn.createStatement().executeQuery("select"
				+" * from usuario u inner join"
				+" endereco e on e.usuario_idusuario = u.idusuario"
				+" inner join mensagem m on m.usuario_idusuario = u.idusuario"
				+" inner join local l on l.usuario_idusuario = u.idusuario"
				+" where e.pais like 'g' and e.estado like 'f' and m.texto like '%i'"
				+" and l.longitude like '%091';");
			Timer.end();
			int count = 0;
			while (rs.next()){
				count++;
			}
			System.out.println("Total mysql: "+Timer.result()+" count: "+count);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		Timer.end();
	}

	@Override
	public void update01() {
		Timer.start();
		try {
			Timer.start();
			int result = this.conn.createStatement().executeUpdate("" +
					"update usuario inner join local on local.usuario_idusuario" +
					" = usuario.idusuario inner join mensagem on mensagem.usuario_idusuario" +
					" = usuario.idusuario inner join endereco on endereco.usuario_idusuario " +
					"= usuario.idusuario set usuario.email = 'teste@teste.com' " +
					"where endereco.pais like 'e' AND mensagem.texto like '%s';");
			Timer.end();
			System.out.println("Total mysql: "+Timer.result()+" count: "+result);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		Timer.end();
	}

}
