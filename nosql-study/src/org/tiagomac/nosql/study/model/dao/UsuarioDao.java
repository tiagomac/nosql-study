package org.tiagomac.nosql.study.model.dao;

import java.util.List;

import org.tiagomac.nosql.study.model.beans.Usuario;

import com.mongodb.DBObject;

public interface UsuarioDao extends DaoGenerico<Usuario> {

	public void deleteAll();

	public List<DBObject> findPrimeiros(int qntd);

	public void getPorLogin(String... nomes);

	public void getSelect01();
	
	public void update01();
}
