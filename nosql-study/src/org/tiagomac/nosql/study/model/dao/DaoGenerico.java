package org.tiagomac.nosql.study.model.dao;

import java.io.Serializable;
import java.util.List;


public interface DaoGenerico<T extends Serializable> {
	
	/**
	 * Insere no banco.
	 * @param T
	 * @return
	 */
	public boolean inserir(T obj);
	/**
	 * Insere no banco uma lista.
	 * @param T
	 * @return
	 */
	public boolean inserirAll(List<T> obj);
	/**
	 * Atualiza no banco.
	 * @param T
	 * @return
	 */
	public boolean atualizar(T obj);
	/**
	 * Exclui no banco.
	 * @param T
	 * @return
	 */
	public boolean excluir(T obj);
	/**
	 * Pesqisa no banco.
	 * @param T
	 * @return
	 */
	public List<T> pesquisar(T obj);
	/**
	 * Lista todos do banco.
	 * @return List<T>
	 */
	public List<T> listar();
	
}
