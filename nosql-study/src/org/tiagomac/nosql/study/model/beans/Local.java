package org.tiagomac.nosql.study.model.beans;

import java.io.Serializable;

import com.mongodb.BasicDBObject;

public class Local implements Serializable {

	private static final long serialVersionUID = -258627507825909951L;
	
	private Long id;
	private String latitude;
	private String longitude;
	private Long timestamp;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getLatitude() {
		return latitude;
	}
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	public String getLongitude() {
		return longitude;
	}
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	public Long getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(Long timestamp) {
		this.timestamp = timestamp;
	}
	/**
	 * mongo database convert to BasicDBObject
	 * @return BasicDBObject
	 */
	public BasicDBObject getBasicDBObject(){
		BasicDBObject dbo = new BasicDBObject();
		dbo.put("latitude", getLatitude());
		dbo.put("longitude", getLongitude());
		dbo.put("timestamp", getTimestamp());
		return dbo;
	}
	
}
