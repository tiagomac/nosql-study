package org.tiagomac.nosql.study.model.beans;

import java.io.Serializable;
import java.util.List;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;

public class Usuario implements Serializable {
	
	private static final long serialVersionUID = 161359256870310653L;
	
	private Long id;
	private String login;
	private String senha;
	private String email;
	private List<Local> local;
	private List<Mensagem> mensagem;
	private List<Usuario> amigos;
	private Long timestamp;
	private Long criacao;
	private Endereco endereco;
	
//	login - representa o login do usu�rio pelo qual ele tamb�m pode ser localizado.
//	senha - senha de acesso do usu�rio na sua conta.
//	email - e-mail do usu�rio para acessar o sistema ou receber atualiza��es do mesmo.
//	list<local> - locais pelo qual o usu�rio j� passou, servindo de hist�rico e para exibir a atual localidade do usu�rio.
//	list<mensagem> - mensagens postadas pelo usu�rio.
//	list<usuario> - usu�rios amigos.
//	timestamp - �ltimo login realizado pelo usu�rio.
//	criacao - timestamp da data de cria��o da conta do usu�rio.
//	endereco - endere�o residencial do usu�rio.

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public List<Local> getLocal() {
		return local;
	}
	public void setLocal(List<Local> local) {
		this.local = local;
	}
	public List<Mensagem> getMensagem() {
		return mensagem;
	}
	public void setMensagem(List<Mensagem> mensagem) {
		this.mensagem = mensagem;
	}
	public List<Usuario> getAmigos() {
		return amigos;
	}
	public void setAmigos(List<Usuario> amigos) {
		this.amigos = amigos;
	}
	public Long getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(Long timestamp) {
		this.timestamp = timestamp;
	}
	public Long getCriacao() {
		return criacao;
	}
	public void setCriacao(Long criacao) {
		this.criacao = criacao;
	}
	public Endereco getEndereco() {
		return endereco;
	}
	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}
	/**
	 * mongo database convert to BasicDBObject
	 * @return BasicDBObject
	 */
	public BasicDBObject getBasicDBObject(){
		BasicDBObject dbo = new BasicDBObject();
//		for (Usuario amigo : usuario.getAmigos()){
//			dbo.put("amigos", getBasicDBObject(amigo));
//		}
		dbo.put("criacao", getCriacao());
		dbo.put("email", getEmail());
		dbo.put("endereco", getEndereco().getBasicDBObject());
		BasicDBList localList = new BasicDBList();
		for (Local local : getLocal()){
			localList.add(local.getBasicDBObject());
		}
		dbo.put("local", localList);
		dbo.put("login", getLogin());
//		BasicDBObject msgs = new BasicDBObject();
		BasicDBList dbList = new BasicDBList();
		for (Mensagem m : getMensagem()){
			dbList.add(m.getBasicDBObject());
		}
		dbo.put("mensagens", dbList);
		dbo.put("senha", getSenha());
		dbo.put("timestamp", getTimestamp());
		return dbo;
	}

}
