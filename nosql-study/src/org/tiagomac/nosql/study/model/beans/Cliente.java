package org.tiagomac.nosql.study.model.beans;

import java.io.Serializable;

public class Cliente implements Serializable {
	
	private static final long serialVersionUID = 7570115082696767573L;
	String nome;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
}
