package org.tiagomac.nosql.study.model.beans;

import java.io.Serializable;

import com.mongodb.BasicDBObject;

public class Mensagem implements Serializable {

	private static final long serialVersionUID = -2274845069204318805L;
	
	private Long id;
	private String texto;
	private Long timestamp;
	private Usuario usuario;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getTexto() {
		return texto;
	}
	public void setTexto(String texto) {
		this.texto = texto;
	}
	public Long getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(Long timestamp) {
		this.timestamp = timestamp;
	}
	public Usuario getUsuario() {
		return usuario;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	/**
	 * mongo database convert to BasicDBObject
	 * @return BasicDBObject
	 */
	public BasicDBObject getBasicDBObject(){
		BasicDBObject dbo = new BasicDBObject();
		if (getId() != null){
			dbo.put("_id",getId());
		}
		dbo.put("texto",getTexto());
		dbo.put("timestamp",getTimestamp());
		return dbo;
	}

}
