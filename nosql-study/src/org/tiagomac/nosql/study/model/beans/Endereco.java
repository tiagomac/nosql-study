package org.tiagomac.nosql.study.model.beans;

import java.io.Serializable;

import com.mongodb.BasicDBObject;

public class Endereco implements Serializable {

	private static final long serialVersionUID = 1L;
	private Long id;
	private String pais;
	private String estado;
	private String cidade;
	private String cep;
	private String logadouro;
	private String complemento;
	private Usuario usuario;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getPais() {
		return pais;
	}
	public void setPais(String pais) {
		this.pais = pais;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getCidade() {
		return cidade;
	}
	public void setCidade(String cidade) {
		this.cidade = cidade;
	}
	public String getCep() {
		return cep;
	}
	public void setCep(String cep) {
		this.cep = cep;
	}
	public String getLogadouro() {
		return logadouro;
	}
	public void setLogadouro(String logadouro) {
		this.logadouro = logadouro;
	}
	public String getComplemento() {
		return complemento;
	}
	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}
	public Usuario getUsuario() {
		return usuario;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	/**
	 * mongo database convert to BasicDBObject
	 * @return BasicDBObject
	 */
	public BasicDBObject getBasicDBObject(){
		BasicDBObject dbo = new BasicDBObject();
		dbo.put("cep",getCep());
		dbo.put("cidade",getCidade());
		dbo.put("complemento",getComplemento());
		dbo.put("estado",getEstado());
		dbo.put("logadouro",getLogadouro());
		dbo.put("pais",getPais());
		return dbo;
	}
}
