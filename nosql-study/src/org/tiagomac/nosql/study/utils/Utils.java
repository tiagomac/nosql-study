package org.tiagomac.nosql.study.utils;

import java.util.HashSet;
import java.util.Iterator;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.math.RandomUtils;

public class Utils {

	public static Iterator<String> getRandomlyNames(int characterLength, final int generateSize) {
		if (characterLength == 1){
			characterLength++;
		}
		HashSet<String> list = new HashSet<String>();
		for (int i = 0; i < generateSize; ++i) {
			String name = null;
			do {
				name = RandomStringUtils.randomAlphabetic(RandomUtils.nextInt(characterLength - 1) + 1);
			} while (list.contains(name));
			list.add(name);
		}
		Iterator<String> it = list.iterator(); 
//		while (it.hasNext()){
//			retorno += it.next();
//		}
		return it;
	}
	
	public static Iterator<String> getRandomlyNamesNumeric(int characterLength, final int generateSize) {
		if (characterLength == 1){
			characterLength++;
		}
		HashSet<String> list = new HashSet<String>();
		for (int i = 0; i < generateSize; ++i) {
			String name = null;
			do {
				name = RandomStringUtils.randomAlphanumeric(RandomUtils.nextInt(characterLength - 1) + 1);
			} while (list.contains(name));
			list.add(name);
		}
		Iterator<String> it = list.iterator(); 
//		while (it.hasNext()){
//			retorno += it.next();
//		}
		return it;
	}
	
	public static Long getRandomlyNumber(final int size) {
		String value = "";
		for (int i = 0 ; i < size ; i++){
			value += Math.round(Math.random()*10);
		}
		return Long.parseLong(value);
	}

}
