package org.tiagomac.nosql.study.utils;

public class Contador {
	
	private static Contador thisObj = null;
	private long startMysql = 0;
	private long startMongodb = 0;
	private long endMysql = 0;
	private long endMongodb = 0;
	
	private Contador(){	}
	
	public static Contador getInstance(){
		if (thisObj == null){
			thisObj = new Contador();
		}
		return thisObj;
	}
	
	public void startMySQL(){
		this.startMysql = System.currentTimeMillis();
	}
	
	public void startMongoDB(){
		this.startMongodb = System.currentTimeMillis();
	}
	
	public void endMySQL(){
		this.endMysql = System.currentTimeMillis();
	}
	
	public void endMongoDB(){
		this.endMongodb = System.currentTimeMillis();
	}
	
	public long getTotalMySQL(){
		return (endMysql - startMysql);
	}
	
	public long getTotalMongoDB(){
		return (endMongodb - startMongodb);
	}

}
