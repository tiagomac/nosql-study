package org.tiagomac.nosql.study.utils;

public class Timer {
	
	private static long start 	= 0;
	private static long end 	= 0;
	private static long result 	= 0;
	
	public static void start(){
		Timer.start = 0; Timer.end = 0; Timer.result = 0;
		Timer.start = System.currentTimeMillis();
	}
	
	public static void end(){
		Timer.end 	= System.currentTimeMillis();
		Timer.result = Timer.end - Timer.start;
	}
	
	public static long result(){
		return Timer.result;
	}

}
