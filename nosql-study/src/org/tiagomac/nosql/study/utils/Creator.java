package org.tiagomac.nosql.study.utils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.tiagomac.nosql.study.model.beans.Endereco;
import org.tiagomac.nosql.study.model.beans.Local;
import org.tiagomac.nosql.study.model.beans.Mensagem;
import org.tiagomac.nosql.study.model.beans.Usuario;

public class Creator {
	
	public static List<Usuario> getRandomUsuarios(long qtdUsario, long qtdLocal, long qtdMensagem) {
		List<Usuario> uList = new ArrayList<Usuario>();
		for (; qtdUsario > 0; qtdUsario--){
			Usuario usuario = new Usuario();
			//usuario.setCriacao(Utils.getRandomlyNumber(11)); // n�meros rand�micos.
			usuario.setCriacao((new Date()).getTime());
			usuario.setEmail(Utils.getRandomlyNames(9, 1).next()
					+"@"+Utils.getRandomlyNames(5,1).next()+"."+Utils.getRandomlyNames(3,1).next()
					+"."+Utils.getRandomlyNames(2, 1).next());
			Endereco end = new Endereco();
			end.setCep(Utils.getRandomlyNumber(5)+"-"+Utils.getRandomlyNumber(3));
			end.setCidade(Utils.getRandomlyNames(1, 5).next());
			end.setComplemento(Utils.getRandomlyNames(1, 20).next());
			end.setEstado(Utils.getRandomlyNames(1, 6).next());
			end.setLogadouro(Utils.getRandomlyNames(1, 10).next());
			end.setPais(Utils.getRandomlyNames(1, 7).next());
			usuario.setEndereco(end);
			usuario.setLogin(Utils.getRandomlyNames(3, 6).next());
			usuario.setSenha(Utils.getRandomlyNames(1, 7).next());
			usuario.setTimestamp((new Date()).getTime());
			List<Local> locais = new ArrayList<Local>();
			for (long qtLocalTmp = qtdLocal; qtLocalTmp > 0; qtLocalTmp--){
				Local l = new Local();
				l.setLatitude(String.valueOf(Utils.getRandomlyNumber(8)));
				l.setLongitude(String.valueOf(Utils.getRandomlyNumber(8)));
				l.setTimestamp((new Date()).getTime());
				locais.add(l);
			}
			usuario.setLocal(locais);
			List<Mensagem> mensagens = new ArrayList<Mensagem>();
			for (long qtMensagemTmp = qtdMensagem; qtMensagemTmp > 0; qtMensagemTmp--){
				Mensagem m = new Mensagem();
				m.setTexto(Utils.getRandomlyNames(140, 1).next());
				m.setTimestamp((new Date()).getTime());
				mensagens.add(m);
			}
			usuario.setMensagem(mensagens);
			uList.add(usuario);
		}
		return uList;
	}
	
	public static Endereco getRandomEndereco() {
		return null;

	}

	public static Local getRandomLocal() {
		return null;

	}

	public static Mensagem getRandomMensagem() {
		return null;

	}

}
