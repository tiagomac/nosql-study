package org.tiagomac.nosql.study.servlets;

import java.io.IOException;
import java.net.UnknownHostException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.Mongo;
import com.mongodb.MongoException;

@WebServlet("/retrieve")
public class RetrieveServlet extends HttpServlet {

	private static final long serialVersionUID = -8967143401512509122L;
	
	private static Mongo m;
	private static DB db;
	private static DBCollection coll;
	
	public RetrieveServlet(){
		try {
			if (RetrieveServlet.m == null)
				RetrieveServlet.m = new Mongo( "localhost" , 27017 );
			if (RetrieveServlet.db == null)
				RetrieveServlet.db = RetrieveServlet.m.getDB( "test" );
			if (RetrieveServlet.coll == null)
				RetrieveServlet.coll = db.getCollection("teste");
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (MongoException e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		doPost(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		long start = System.currentTimeMillis();
//		RetrieveServlet.db.requestStart();
		DBCursor cur = RetrieveServlet.coll.find();
		System.out.println(cur.count());
//		RetrieveServlet.db.requestDone();
//        while(cur.hasNext()) {
//            cur.next();
//        }
        long end = System.currentTimeMillis();
        long total = (end - start);
        
        System.out.println("Retrieve total: "+total);
		resp.getWriter().println("finalizou retrieve");
	}
	
}
