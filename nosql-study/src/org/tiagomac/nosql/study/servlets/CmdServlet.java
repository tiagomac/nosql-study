package org.tiagomac.nosql.study.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.tiagomac.nosql.study.facade.StudyFcd;
import org.tiagomac.nosql.study.model.dao.UsuarioDao;
import org.tiagomac.nosql.study.utils.Timer;

@WebServlet("/cmd")
public class CmdServlet extends HttpServlet {
	
	private UsuarioDao usuarioDao = null;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		String cmd = req.getParameter("cmd");
		if (cmd == null){
			resp.getWriter().println("comando desconhecido!");
			resp.flushBuffer();
			return;
		}
		if (cmd.equalsIgnoreCase("inserir")){
			StudyFcd.getInstance().inserirUsuarios(100, 800, 800);
		}
		if (cmd.equalsIgnoreCase("deletarTodos")){
			StudyFcd.getInstance().deletarTodosUsuarios();
		}
		if (cmd.equalsIgnoreCase("primeiros")){
			StudyFcd.getInstance().getPrimeiros(200);
			System.out.println("result: "+Timer.result());
		}
		
		if (cmd.equalsIgnoreCase("todos")){
			StudyFcd.getInstance().deletarTodosUsuarios();
			StudyFcd.getInstance().inserirUsuarios(100, 800, 800);
			StudyFcd.getInstance().deletarTodosUsuarios();
		}
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
	}
	

}
