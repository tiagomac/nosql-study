package org.tiagomac.nosql.study.servlets;

import java.io.IOException;
import java.net.UnknownHostException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.Mongo;
import com.mongodb.MongoException;
import com.mongodb.WriteResult;

@WebServlet("/insert")
public class InsertServlet extends HttpServlet {

	private static final long serialVersionUID = -3194077793835841429L;
	
	private static Mongo m = null;
	private static DB db = null;
	private static DBCollection coll = null;
	
	public InsertServlet(){
		try {
			if (InsertServlet.m == null)
				InsertServlet.m = new Mongo( "localhost" , 27017 );
			if (InsertServlet.db == null)
				InsertServlet.db = InsertServlet.m.getDB( "test" );
			if (InsertServlet.coll == null)
				InsertServlet.coll = InsertServlet.db.getCollection("teste");
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (MongoException e) {
			e.printStackTrace();
		}

	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		doPost(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		long start = System.currentTimeMillis();
		BasicDBObject doc = null;
		WriteResult res = null;
//		for (String s : Utils.getRandomlyNames(9, 10)){
//			doc = new BasicDBObject();
//	        doc.put("nome", s);
//	        BasicDBObject info = new BasicDBObject();
//
//	        info.put("x", 203);
//	        info.put("y", 102);
//
//	        doc.put("info", info);
	        //InsertServlet.db.requestStart();
//	        res = InsertServlet.m.getDB( "test" ).getCollection("teste").insert(doc);
	        //InsertServlet.db.requestDone();
//		}
		long end = System.currentTimeMillis();
		long total = (end - start);
        
		System.out.println("Inser total: "+total);
        resp.getWriter().println("finalizou insert");
	}
	

}
