package org.tiagomac.nosql.study.facade;

import org.tiagomac.nosql.study.bo.StudyBo;


/**
 * Facade
 * @author tiagocunha
 *
 */
public class StudyFcd {
	
	private static StudyFcd thisInstance = null;
	private StudyBo bo;
	
	private StudyFcd() {
		try {
			setBo(new StudyBo());
		} catch (Exception e) {
			//TODO: tratamentos de erro.
			e.printStackTrace();
		}
	}
	
	public static StudyFcd getInstance(){
		if (thisInstance == null){
			thisInstance = new StudyFcd();
		}
		return thisInstance;
	}

	private void setBo(StudyBo bo) {
		if (this.bo == null){
			this.bo = bo;
		}else{
			System.out.println("erro: bo j� inst�nciado.");
		}
	}
	
	public void inserirUsuarios(long qtdUsuario, long qtdLocal, long qtdMensagem){
		try {
			this.bo.inserirUsuarios(qtdUsuario, qtdLocal, qtdMensagem);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void deletarTodosUsuarios(){
		try{
			this.bo.deletarTodosUsuarios();
		}catch (Exception e){
			e.printStackTrace();
		}
	}
	
	public void getPrimeiros(int qntd){
		try{
			this.bo.getPrimeiros(qntd);
		}catch (Exception e){
			e.printStackTrace();
		}
	}
	
	public void getPorLogin(String... nomes){
		try{
			this.bo.getPorLogin(nomes);
		}catch (Exception e){
			e.printStackTrace();
		}
	}

	public void getSelect01() {
		this.bo.getSelect01();
	}
	
	public void update01(){
		this.bo.update01();
	}
}
