CREATE TABLE usuario (
  usuario_id INTEGER UNSIGNED  NOT NULL   AUTO_INCREMENT,
  login VARCHAR(20)  NULL  ,
  senha VARCHAR(20)  NULL  ,
  email VARCHAR(120)  NULL  ,
  timestamp TIMESTAMP  NULL  ,
  criacao TIMESTAMP  NULL    ,
PRIMARY KEY(usuario_id));


CREATE TABLE mensagem (
  mensagem_id INTEGER UNSIGNED  NOT NULL   AUTO_INCREMENT,
  usuario_usuario_id INTEGER UNSIGNED  NOT NULL  ,
  texto VARCHAR(255)  NULL  ,
  timestamp TIMESTAMP  NULL    ,
PRIMARY KEY(mensagem_id),
  FOREIGN KEY(usuario_usuario_id)
    REFERENCES usuario(usuario_id)
      ON DELETE NO ACTION
      ON UPDATE NO ACTION);


CREATE TABLE endereco (
  endereco_id INTEGER UNSIGNED  NOT NULL   AUTO_INCREMENT,
  usuario_usuario_id INTEGER UNSIGNED  NOT NULL  ,
  pais VARCHAR(20)  NULL  ,
  estado VARCHAR(20)  NULL  ,
  cidade VARCHAR(20)  NULL  ,
  cep VARCHAR(20)  NULL  ,
  logadouro VARCHAR(120)  NULL  ,
  complemento VARCHAR(255)  NULL    ,
PRIMARY KEY(endereco_id),
  FOREIGN KEY(usuario_usuario_id)
    REFERENCES usuario(usuario_id)
      ON DELETE NO ACTION
      ON UPDATE NO ACTION);


CREATE TABLE local (
  local_id INTEGER UNSIGNED  NOT NULL   AUTO_INCREMENT,
  usuario_usuario_id INTEGER UNSIGNED  NOT NULL  ,
  latitude VARCHAR(20)  NULL  ,
  longitude VARCHAR(20)  NULL  ,
  timestamp TIMESTAMP  NULL    ,
PRIMARY KEY(local_id),
  FOREIGN KEY(usuario_usuario_id)
    REFERENCES usuario(usuario_id)
      ON DELETE NO ACTION
      ON UPDATE NO ACTION);


CREATE TABLE usuario_has_usuario (
  usuario_usuario_id INTEGER UNSIGNED  NOT NULL    ,
  FOREIGN KEY(usuario_usuario_id)
    REFERENCES usuario(usuario_id),
usuario_amigo_id INTEGER UNSIGNED  NOT NULL    ,
  FOREIGN KEY(usuario_amigo_id)
    REFERENCES usuario(usuario_id));


